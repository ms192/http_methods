package az.ingress.demo.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String name;

    @Column(name = "surname")
    String surName;

    Integer age;

    private static Student instance;

    //Singleton pattern
    public static Student getInstance(){
        if (instance == null){
            synchronized (Student.class) {
                if (instance == null){
                    instance = new Student();
                }
            }
        }
        return instance;
    }
}
