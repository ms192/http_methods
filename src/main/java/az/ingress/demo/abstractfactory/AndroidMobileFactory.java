package az.ingress.demo.abstractfactory;

public class AndroidMobileFactory extends AbstractFactory{
    @Override
    public IMobile getMobile (String mobileModel){
        if (mobileModel.equalsIgnoreCase("Oneplus")) {
            return new OnePlus();
        } else if (mobileModel.equalsIgnoreCase("Sony")) {
            return new Sony();
        }
        return null;
    }
}
