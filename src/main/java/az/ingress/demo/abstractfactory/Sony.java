package az.ingress.demo.abstractfactory;

public class Sony implements IMobile{

    @Override
    public void brandName() {
        System.out.println("The brand name is Sony");
    }
}
