package az.ingress.demo.abstractfactory;

public class AbstractFactoryPatternDemo {
    AbstractFactory abstractFactory1 = MobileFactoryProducer.getFactory(false);
    IMobile onePlus = abstractFactory1.getMobile("OnePlus");
        //onePlus
    IMobile sony = abstractFactory1.getMobile("Sony");
        //sony.brandName();
    AbstractFactory abstractFactory2 = MobileFactoryProducer.getFactory(true);
    IMobile iphone = abstractFactory2.getMobile("Iphone");
        //iphone.brandName();


}
