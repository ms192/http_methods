package az.ingress.demo.abstractfactory;

public class OnePlus implements IMobile{
    @Override
    public void brandName() {
        System.out.println("The brand name is OnePlus");
    }
}
