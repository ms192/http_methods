package az.ingress.demo.abstractfactory;

public abstract class AbstractFactory {
    abstract public IMobile getMobile(String mobileModel);
}
