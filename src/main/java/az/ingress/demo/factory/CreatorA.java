package az.ingress.demo.factory;

public class CreatorA extends Creator{
    @Override
    public Product createProduct() {
        return new ProductA();
    }
}
