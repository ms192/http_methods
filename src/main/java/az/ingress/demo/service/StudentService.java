package az.ingress.demo.service;

import az.ingress.demo.model.Student;

import java.util.List;

public interface StudentService {

    Student get(Integer id);
    List<Student> getAll();

    Student create(Student student);

    Student update(Integer id, Student student);


    void delete(Integer id);
}
