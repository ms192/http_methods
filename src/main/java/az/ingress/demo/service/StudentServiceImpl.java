package az.ingress.demo.service;

import az.ingress.demo.model.Student;
import az.ingress.demo.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService{

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student get(Integer id){
        log.info("student service get method");
        Optional<Student> student = studentRepository.findById(id);
        if (student.isEmpty()) {
            throw new RuntimeException("Student not found");
        }
        return student.get();
    }
    @Override
    public List<Student> getAll(){
        log.info("student service getAll method");
        List<Student> students = studentRepository.findAll();
        if (students.isEmpty()) {
            throw new RuntimeException("Students not found");
        }
        return students;
    }
    @Override
    public Student create(Student student){
        log.info("student service create method");
        Student studentInDb = studentRepository.save(student);
        return studentInDb;
    }
    @Override
    public Student update(Integer id, Student student){
        log.info("student service update method");
        Student entity = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setSurName(student.getSurName());
        entity = studentRepository.save(entity);
        return entity;
    }
    @Override
    public void delete(Integer id){
        log.info("student service delete method");
        studentRepository.deleteById(id);
    }
}
